//
//  ValueCommon.h
//  ValueCommon
//
//  Created by James Cash on 23-09-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ValueCommon.
FOUNDATION_EXPORT double ValueCommonVersionNumber;

//! Project version string for ValueCommon.
FOUNDATION_EXPORT const unsigned char ValueCommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ValueCommon/PublicHeader.h>



//
//  CommonConstants.swift
//  ValueDisplay
//
//  Created by James Cash on 23-09-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

import Foundation

public let groupId = "group.occasionallycogent.testing"
public let valueKey = "lighthousedemovaluekey"

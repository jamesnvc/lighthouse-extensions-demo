//
//  ViewController.swift
//  ValueDisplay
//
//  Created by James Cash on 23-09-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

import UIKit
import ValueCommon

class ViewController: UIViewController {
    @IBOutlet weak var valueLabel: UILabel!

    @IBOutlet weak var valueSlider: UISlider!
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshData()
    }
    @IBAction func valueChanged(sender: UISlider) {
        let newValue = sender.value
        NSUserDefaults(suiteName: groupId)!.setFloat(newValue, forKey: valueKey)
        valueLabel.text = "\(newValue)"
    }

    func refreshData() {
        let oldValue = NSUserDefaults(suiteName: groupId)!.floatForKey(valueKey)
        valueSlider.value = oldValue
        valueLabel.text = "\(oldValue)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


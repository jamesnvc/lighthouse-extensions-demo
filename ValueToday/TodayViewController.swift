//
//  TodayViewController.swift
//  ValueToday
//
//  Created by James Cash on 23-09-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

import UIKit
import NotificationCenter
import ValueCommon

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet weak var valueSlider: UISlider!
    override func viewDidLoad() {
        super.viewDidLoad()
        let oldValue = NSUserDefaults(suiteName: groupId)!.floatForKey(valueKey)
        valueSlider.value = oldValue
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func valueChanged(sender: UISlider) {
        let newValue = sender.value
        NSUserDefaults(suiteName: groupId)!.setFloat(newValue, forKey: valueKey)
    }

    func widgetPerformUpdateWithCompletionHandler(completionHandler: ((NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.

        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData

        completionHandler(NCUpdateResult.NewData)
    }
    
}

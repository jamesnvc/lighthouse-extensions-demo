//
//  ShareViewController.swift
//  ValueShare
//
//  Created by James Cash on 23-09-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

import UIKit
import Social
import ValueCommon

class ShareViewController: SLComposeServiceViewController {

    override func isContentValid() -> Bool {
        // Do validation of contentText and/or NSExtensionContext attachments here
        let formatter = NSNumberFormatter()
        return formatter.numberFromString(contentText) != nil
    }

    override func didSelectPost() {
        // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
    
        // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
        let formatter = NSNumberFormatter()
        let newValue = formatter.numberFromString(contentText)!.floatValue
        NSUserDefaults(suiteName: groupId)!.setFloat(newValue, forKey: valueKey)
        self.extensionContext!.completeRequestReturningItems([], completionHandler: nil)
    }

    override func configurationItems() -> [AnyObject]! {
        // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
        return []
    }

}
